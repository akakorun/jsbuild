import angular from "angular";
import uiRouter from "angular-ui-router";

window.app = angular.module('app', [
  uiRouter,
]);

require('./app/app');
require('./app/routes');
