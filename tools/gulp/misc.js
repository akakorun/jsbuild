import path from "path";
import gulp from 'gulp';
import gutil from 'gulp-util';
import del from 'del';
import fs from 'fs-extra';
import _ from 'lodash';

import {copyPaths, paths} from '../config';

export function gulpWatch() {
  return gulp.watch(paths.scssBatch, ['sass'])
}

export function gulpAssets() {
  let assets = gulp.src(paths.assets)
    .pipe(gulp.dest(paths.dist + '/assets'));

  gulpCopy();

  return assets;
  // let copy = gulpCopy();
  // return es.merge(assets, copy);
}

// todo: handle copy without gulp stream
export function gulpCopy() {
  _.map(copyPaths, (dest, source) => fs.copySync(path.join(paths.root, source), dest));
}

export function gulpClean(cb) {
  del([paths.dist]).then(function (paths) {
    gutil.log("[clean]", paths);
    cb();
  })
}
