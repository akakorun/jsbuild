import path from "path";
import _ from "lodash";

let rootPath = process.cwd();
let clientPath = path.join(rootPath, 'client');
let distPath = path.join(rootPath, '.dist');

// todo: add check if file exists so we can use only the defaults
let configFile = require(path.join(rootPath, 'jsbuild.config.js'));

let paths = _.assign({
  root: rootPath,
  dist: distPath,
  client: clientPath,
  assets: path.join(clientPath, 'assets/**/*'),
  assetsEntry: path.join(clientPath, 'assets/'),
  scssEntry: path.join(clientPath, 'index.scss'),
  scssBatch: path.join(clientPath, '/**/*.scss'),
  jsEntry: path.join(clientPath, 'index.js'),
  htmlEntry: path.join(clientPath, 'index.html')
}, configFile.paths);

let copyPaths = _.assign({
  'node_modules/semantic-ui-css/themes/': path.join(paths.dist, 'themes'),
  'node_modules/font-awesome/fonts': path.join(paths.dist, 'fonts')
}, configFile.copyPaths);

let server = _.assign({
  staticRoutes: {
    "/node_modules": "node_modules",
    "/themes": "node_modules/semantic-ui-css/themes",
    "/fonts": "node_modules/font-awesome/fonts"
  },
  proxy: {
    '/api': {
      target: 'http://192.168.100.38:8000/',
      changeOrigin: true
    },
  }
}, configFile.server);

export {
  paths,
  copyPaths,
  server,
}
