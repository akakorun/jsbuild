## Configuration
* Improve ways to specify user configuration
    * File configuration
        * ~~jsbuild.config.js runtime loading~~
        * json / yaml config
    * Instance configuration
    * Console configuration -- when console is available
* Webpack
    * Transfer sass from gulp to webpack
    * Configurations
        * Adapt to Webpack2 better
        * Rewrite
